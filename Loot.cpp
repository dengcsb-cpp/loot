#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

struct Item
{
	string name;
	int gold;
};

const int TARGET_GOLD = 500;
const int DUNGEON_FEE = 25;
const int STARTING_GOLD = 50;

Item* generateRandomItem();
void printItem(Item* item);
void enterDungeon(int& gold);

int main()
{
	// Gold should be declared here. With pointers/reference, we can pass this around.
	int gold = STARTING_GOLD;
	vector<Item*> items;

	while (gold >= DUNGEON_FEE)
	{
		// Pass in the address of gold as parameter of the function
		enterDungeon(gold);
	}

	// Evaluate result
	system("cls");
	cout << "Gold: " << gold << endl;
	if (gold >= TARGET_GOLD) cout << "You win!" << endl;
	else cout << "You lose" << endl;

	system("pause");
	return 0;
}

// Create dynamically allocated Item struct. Remember that only the 'memory address' is being returned since this is a pointer.
Item* generateRandomItem()
{
	// I didn't use an array so that I only need to allocate the randomized item (not the entire pool)
	int r = rand() % 5;
	Item* item = new Item;

	switch (r)
	{
	case 0:
		item->name = "Cursed Stone";
		item->gold = 0;
		break;
	case 1:
		item->name = "Jellopy";
		item->gold = 5;
		break;
	case 2:
		item->name = "Thick Leather";
		item->gold = 25;
		break;
	case 3:
		item->name = "Sharp Talon";
		item->gold = 50;
		break;
	case 4:
		item->name = "Mithril Ore";
		item->gold = 100;
		break;
	default:
		break;
	}

	return item;
}

// With pointer, only the memory address is being passed (4 bytes). Inside the function, we dereference this pointer so that we can access the actual struct. This is very efficient since we're always passing 4 bytes no matter how big the data structure is.
void printItem(Item* item)
{
	cout << item->name << ": " << item->gold << "g" << endl;
}

void enterDungeon(int& gold)
{
	if (gold < DUNGEON_FEE) throw exception("You must have enough gold to enter the dungeon");

	int roundGold = 0;
	int bonus = 1;
	gold -= DUNGEON_FEE;

	while (true)
	{
		system("cls");

		Item* item = generateRandomItem();
		cout << "Gold farmed for current run: " << roundGold << endl;
		cout << "Gold multiplier: x" << bonus << endl;
		cout << "Looting..." << endl;
		system("pause");
		printItem(item);

		// Check loot and resolve result
		if (item->name == "Cursed Stone")
		{
			cout << "Touching the Cursed Stone will kill you. All earnings lost for this dungeon run." << endl;
			delete item; // Since we are returning early, we need to delete the allocated memory.
			return;
		}

		roundGold += item->gold * bonus;
		bonus++;

		// Delete the memory used for the item since we're done with it
		delete item;
		//item = NULL; // This is optional for our current scenario. This will only be required if you plan to use the same variable later (we don't want to keep a reference to a dead address

		// Loot again?
		int choice;
		cout << "Going deeper into the dungeon increases quality of loot. Do you want to continue looting? (0/1)";
		cin >> choice;
		if (choice == 0)
		{
			gold += roundGold;
			return; // We're safe here since we've already deleted the memory for Item
		}
	}
}
